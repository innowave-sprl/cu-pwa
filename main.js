console.log('Hello CU');

const socialDiv = document.querySelector('#social');

let socialNetworks = [
    {id: 1, name: 'Notre chaine Youtube', description: 'Abonnez-vous à notre page!', url: 'https://facebook.com/'},
    {id: 2, name: 'Notre page Facebook', description: 'Abonnez-vous à notre chaine!', url: 'https://youtube.com/'},
    {id: 3, name: 'Notre compte Twitter', description: 'Suivez-nous sur Twitter', url: 'https://twitter.com/'},
    {id: 4, name: 'Notre chaine Twitch', description: 'Supportez-nous sur Twitch', url: 'https://twitch.com'}
];

function loadSocialNetworks(socialNetworks) {
    const allSocialNetworks = socialNetworks
        .map(t => `<div><b>${t.name}</b> ${t.description} - <a href = ${t.url}> site officiel </a></div>`)
        .join('');

    socialDiv.innerHTML = allSocialNetworks; 
}

loadSocialNetworks(socialNetworks);


/*
console.log('Hello CU');
const socialDiv = document.querySelector('#social');

function loadSocialNetworks() {
    fetch('http://localhost:3001/socialNetworks')
        .then(response => {
            response.json()
                .then(socialNetworks => {
                    const allSocialNetworks = socialNetworks.map(t => `<div><b>${t.name}</b> ${t.description}  <a href="${t.url}">site de ${t.name}</a> </div>`)
                            .join('');
            
                    socialDiv.innerHTML = allSocialNetworks; 
                });
        })
        .catch(console.error);
}

loadSocialNetworks();*/

//Registering the SW
if(navigator.serviceWorker){
   navigator.serviceWorker.register('sw.js')   
    .catch(err => console.error);
   }

   //Non persistent notification
   if(window.Notification && window.Notification !== 'denied'){
       Notification.requestPermission(perm => {
           if(perm === 'granted'){
               const options = {
                   body: 'Abonnez-vous!',
                   icon: 'images/icons/icon-72x72.png'
               };
           }
       })
   }