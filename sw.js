console.log('SW On');
const cacheName = 'cache-sw-1.0';

//install and adding cache
/*self.addEventListener('install', evt => {
  console.log('Install evt', evt);
  let cachePromise = caches.open(cacheName).then(cache => {
      return cache.addAll([
        'index.html',
        'main.js',
        'style.css',
        'vendors/bootstrap4.min.css',
        'views/contact.html',
        'views/about.html',
        'public/scripts/about.js',
        'public/scripts/contact.js',
      ])
      .then(console.log('Init Cache'))
      .catch(console.err);
  });
    
   evt.waitUntil(cachePromise);
});
*/

self.addEventListener('install', (evt) => {
    console.log(`sw install at ${new Date().toLocaleTimeString()}`);
    let cachePromise = caches.open(cacheName).then(cache => {
        return cache.addAll([
           'index.html',
           'main.js',
           'style.css',
           'vendors/bootstrap4.min.css',
           'views/contact.html',
           'views/about.html',
           'public/scripts/about.js',
           'public/scripts/contact.js',
        ])
            .then(console.log('cache initialized'))
            .catch(console.err);
    });

    evt.waitUntil(cachePromise);

});



self.addEventListener('activate', evt => {
    console.log('Activate evt', evt);
    let cacheCleanedPromise = caches.keys().then(keys => {
        keys.forEach(key => {
            if (key !== cacheName) {
                return caches.delete(key);
            }
        });
    });
    evt.waitUntil(cacheCleanedPromise);
});

//self.addEventListener('fetch', evt => {

/*    if(!navigator.online){
        const headers = { headers: {
            'content-type': 'text/html;charset-utf-8'
        }};
        evt.respondWith(new Response('<h1>Il semble que vous soyez hors ligne!</h1>', headers));
    }*/
//    console.log('Fetch event on url', evt.request.url);
  
/*
    //Caching only with network fallback
    evt.respondWith(
    caches.match(evt.request).then(res=> {
        console.log(`Cached URL : ${evt.request.url}`, res);
        
        if(res){
            return res;
        }
        return fetch(evt.request).then(newResponse => {
            console.log(`fetched URL : ${evt.request.url}`, newResponse);
            
            caches.open(cacheName).then(cache => cache.put(evt.request, newResponse));
            return  newResponse.clone();
        });
    })
    );
});
*/

// network first strategy

self.addEventListener('fetch', evt => {
    console.log('evt', evt);
    // to prevent this error when posting a form: 
    // "Uncaught (in promise) TypeError: Request method 'POST' is unsupported at caches.open.then.cache"
    if(evt.request.method === 'POST') {
        return;
    }
    evt.respondWith(
        fetch(evt.request).then(res => {
            // we add the latest version into the cache
            caches.open(cacheName).then(cache => cache.put(evt.request, res));
            // we clone it as a response can be read only once (it's like a one time read stream)
            return res.clone();
        })
            .catch(err => caches.match(evt.request))
    );
});

//Persistant notification
self.registration.showNotification('Abonnez vous!', {
    body: 'Ajouter votre companion à votre HomeScreen',
    actions: [
        {action: 'accept', title: 'accepter'},
        {action: 'refuse', title: 'refuser'}
    ]
});

self.addEventListener('notificationclose', evt => {
    console.log('notification closed', evt);
});

self.addEventListener('notificationclick', evt => {
    if(evt.action === 'accept') {
        console.log('Accepted');
    } else if(evt.action === 'refuse') {
        console.log('refused')
    } else {
        console.log('click on the notification itself')
    }
    evt.notification.close();
});

//Push notification
self.addEventListener('push', evt =>  {
    console.log(evt);
    console.log('push  notification sent from dev tools : ', evt.data.text())
    var title = evt.data.text();
    evt.waitUntil(self.registration.showNotification(title, { body: 'Nouvel article en ligne! :)', image: 'images/icons/icon-152x152.png'}));
});